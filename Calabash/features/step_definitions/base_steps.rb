Dado(/^que Eu (?:arrastei|arrasto) a tela para (baixo|cima|esquerda|direita) ate ver o elemento "(.*?)"$/) do |direction, element|
  @page.drag_until_element_is_visible_with_special_query direction.to_sym, element
end

Dado(/^que Eu estou em uma pagina que contem '(.*?)'$/) do |page_text|
  @page.is_on_page? page_text
end

Quando(/^Eu arrasto a tela para (esquerda|direita|baixo|cima) (\d+) vezes$/) do |direction, times|
  @page.drag_for_specified_number_of_times(direction.to_sym, times.to_i)
end

Quando(/^Eu (?:clico|cliquei) no elemento "(.*?)"$/) do |element|
  @page.touch_screen_element element
end

Quando(/^Eu arrasto a tela para (baixo|cima|esquerda|direita)$/) do |direction|
  @page.drag_to direction.to_sym
end

Quando(/^(?:Eu |)reiniciar o aplicativo$/) do
  @page.restart_app
end

Então(/^Eu (?:espero|esperei) ate a barra de progresso sumir$/) do
  # wait_for_progress is a method of the base class, so doesn't matter what is
  # the value of the @page variable, because all screens will have this method
  @page.wait_for_progress
end

Então(/^Eu deveria ver a pagina '(.*?)'$/) do |page_text|
  @page.is_on_page? page_text
end

Então(/^Eu deveria ver uma pagina que contem '(.*?)'$/) do |page_text|
  @page.is_on_page? page_text
end


Então(/^faco um print$/) do
  screenshot_embed
end

Dado(/^que eu estou na tela "([^"]*)"$/) do |screen|
      @base = page(BaseScreen)
      @page = page(@base.screenConvertToId(screen))
      @page.assertScreen
end

Quando(/^eu clico em "([^"]*)"$/) do |button|
    @base.clicarbtn(@page.convertToId(button))
end

Quando(/^eu insiro "([^"]*)" no campo "([^"]*)"$/) do |valor_txt, field_txt|
    @base.preencherForm(valor_txt, @page.convertToId(field_txt))
end

Então(/^sou direcionado para a tela "([^"]*)"$/) do |screen|
    @page = page(@base.screenConvertToId(screen))
    @page.assertScreen
end

Então(/^vejo o texto "([^"]*)"$/) do |txt|
    @base.assertText(txt)
end

Quando(/^eu seleciono "([^"]*)" scrollando para "([^"]*)"$/) do |option_selected, direction|
    @base.touchElement(@page.convertToId(option_selected))
    @page.drag_until_element_is_visible_and_touch_it direction.to_sym, option_selected

end

Quando(/^espero o carregamento da tela que contem "([^"]*)"$/) do |txt|
    @base.loadingScreen(txt)
end

