class BaseScreen < AndroidScreenBase

# Declare todos os elementos de botoes aqui
    element(:btn_registerEmail)        { 'buttonConnectWithMoneyLover' }

    element(:edit_txtEmail)        { 'etEmail' }

def clicarbtn(btn)
    tap_when_element_exists("* id:'#{btn}'")
    sleep(2)
end

def preencherForm(txt_value, edit_field)
    touch_screen_element edit_field
    keyboard_enter_text(txt_value)
    hide_soft_keyboard()
end

def assertText(text)
     wait_for_text(text, timeout: 5)
end

def touchElement(selector)
    touch_screen_element(selector)
end

def loadingScreen(element)
    wait_for_text(element)
end

def screenConvertToId( element )
         case element
             when "Inicial"
                screen = WelcomeScreen
             when "Login"
                screen = LoginScreen
             when "Cadastro"
                screen = SubscriptionScreen
             when "Selecao Moeda"
                screen = CurrencypickerScreen
             when "Drawer"
                screen = DrawerlayoutScreen
         end

        return screen
end


end


