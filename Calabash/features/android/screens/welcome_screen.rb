class WelcomeScreen < AndroidScreenBase
  #Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)              { 'welcome_fragment' }
  element(:btn_registerEmail)        { 'buttonConnectWithMoneyLover' }
  element(:btn_registerGoogle)       { 'buttonConnectGoogle' }
  element(:btn_registerFacebook)     { 'buttonConnectFacebook' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end

  def assertScreen
         ids = [ btn_registerEmail, btn_registerGoogle, btn_registerFacebook]
             ids.each do |txt|
                check_element_exists("* id:'#{txt}'")
             end
  end

  def convertToId(element)
         case element
             when "Register or Sign in by Email"
                btn = btn_registerEmail
          end
        return btn
  end

end
