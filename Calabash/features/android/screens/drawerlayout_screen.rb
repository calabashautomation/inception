class DrawerlayoutScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'drawer_layout' }
  element(:saldo_reais)         { 'wallet_balance' }
  element(:menu_item)           { 'design_menu_item_text' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end

  def assertScreen
         ids = [ layout_name, saldo_reais, menu_item]
             ids.each do |txt|
                check_element_exists("* id:'#{txt}'")
             end
  end


end
