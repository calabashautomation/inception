class LoginScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'form_login' }
  element(:edit_Email)          { 'etEmail' }
  element(:edit_Password)          { 'etPassword' }
  element(:btn_Login)           { 'login' }
  element(:txt_ForgotPassword)  { 'forgot_password' }
  element(:toggle_Password)     { 'text_input_password_toggle' }
  element(:tab_Login)           { 'tab_login' }
  element(:tab_Register)        { 'tab_register' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end

  def assertScreen
         ids = [ layout_name, edit_Email, btn_Login, txt_ForgotPassword, toggle_Password, tab_Login, tab_Register]
             ids.each do |txt|
                check_element_exists("* id:'#{txt}'")
             end
  end


  def convertToId(element)
           case element
               when "REGISTER TAB"
                  btn = tab_Register
               when "LOGIN TAB"
                  btn = tab_Login
               when "LOGIN"
                  btn = btn_Login
               when "email_login"
                  btn = edit_Email
               when "senha_login"
                  btn = edit_Password
            end
          return btn
  end


end
