class CurrencypickerScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'currency_picker' }
  element(:btn_GetStarted)      { 'done' }
  element(:picker_moedas)       { 'currency_picker' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end

   def assertScreen
          ids = [ layout_name, btn_GetStarted]
              ids.each do |txt|
                 check_element_exists("* id:'#{txt}'")
              end
   end


   def convertToId(element)
        case element
            when "LET'S GET STARTED!"
               convertedId = btn_GetStarted
            when "Reais"
               convertedId = picker_moedas
         end
        return convertedId
   end


end
