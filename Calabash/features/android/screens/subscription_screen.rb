class SubscriptionScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'form_register' }
  element(:edit_Email)          { 'etEmail' }
  element(:edit_Password)       { 'etPassword' }
  element(:toggle_Password)     { 'text_input_password_toggle' }
  element(:btn_Register)        { 'btnRegister' }
  element(:tab_Register)        { 'tab_register' }
  element(:tab_Login)           { 'tab_login' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end


  def assertScreen
         ids = [ layout_name, edit_Email, edit_Password, toggle_Password, btn_Register, tab_Register, tab_Login]
             ids.each do |txt|
                check_element_exists("* id:'#{txt}'")
             end
  end

  def convertToId(element)
       case element
            when "email_register"
                edit = edit_Email
            when "senha_register"
                edit = edit_Password
            when "REGISTER"
                edit = btn_Register
        end
       return edit
  end


end
