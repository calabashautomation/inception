# language: pt
Funcionalidade: Login 

  Contexto:
    # Insira os passos
    Dado que eu estou na tela "Inicial"
    Quando eu clico em "Register or Sign in by Email"
    Então sou direcionado para a tela "Login"


  @login
  Cenário: 01 Login com Sucesso
    # Insira os passos
  Quando eu insiro "tst5845@mailinator.com" no campo "email_login"
  E eu insiro "123456" no campo "senha_login"
  E eu clico em "LOGIN"
  E espero o carregamento da tela que contem "Go Premium"
  Então sou direcionado para a tela "Drawer"
  E vejo o texto "Transactions"

  @login
  @reinstall
  Cenário: 02 Login com senha errada

  Quando eu insiro "tst5845@mailinator.com" no campo "email_login"
  E eu insiro "456789" no campo "senha_login"
  E eu clico em "LOGIN"
  Então vejo o texto "Invalid email/password combination. Please try again."
