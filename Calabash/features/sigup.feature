# language: pt
# encoding: utf-8
Funcionalidade: Sigup

  Contexto:
    Dado que eu estou na tela "Inicial"
    Quando eu clico em "Register or Sign in by Email"
    Então sou direcionado para a tela "Login"
    Quando eu clico em "REGISTER TAB"
    Então sou direcionado para a tela "Cadastro"

  @register
  Cenário: 01 SigUp App
    # Insira os passos


  Quando eu insiro "tst6650@mailinator.com" no campo "email_register"
  E eu insiro "123456" no campo "senha_register"
  E eu clico em "REGISTER"
  E espero o carregamento da tela que contem "Choose a default currency"
  Então sou direcionado para a tela "Selecao Moeda"
  E vejo o texto "Choose a default currency"
  Quando eu seleciono "Reais" scrollando para "baixo"
  E eu clico em "LET'S GET STARTED!"
  E espero o carregamento da tela que contem "Go Premium"
  Então sou direcionado para a tela "Drawer"
  E vejo o texto "Transactions"


  @reinstall
  @register
  Cenário: 02 SigUp App com um e-mail já cadastrado

  Quando eu insiro "tst6650@mailinator.com" no campo "email_register"
  E eu insiro "123456" no campo "senha_register"
  E eu clico em "REGISTER"
  Então vejo o texto "This email is unavailable. Please choose another one."








